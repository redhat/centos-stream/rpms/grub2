From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: dann frazier <dann.frazier@canonical.com>
Date: Thu, 25 Aug 2022 17:08:09 -0600
Subject: [PATCH] linuxefi: Invalidate i-cache before starting the kernel

We need to flush the memory range of the code we are about to execute
from the instruction cache before we can safely execute it. Not doing
so appears to be the source of rare synchronous exceptions a user
is seeing on a Cortex-A72-based platform while executing the Linux EFI
stub. Notably they seem to correlate with an instruction on a cache
line boundary.

Signed-off-by: dann frazier <dann.frazier@canonical.com>
---
 grub-core/loader/efi/linux.c | 4 ++++
 1 file changed, 4 insertions(+)

diff --git a/grub-core/loader/efi/linux.c b/grub-core/loader/efi/linux.c
index 5889e3f36f8..21a6298b087 100644
--- a/grub-core/loader/efi/linux.c
+++ b/grub-core/loader/efi/linux.c
@@ -16,6 +16,7 @@
  *  along with GRUB.  If not, see <http://www.gnu.org/licenses/>.
  */
 
+#include <grub/cache.h>
 #include <grub/charset.h>
 #include <grub/command.h>
 #include <grub/err.h>
@@ -281,6 +282,9 @@ grub_efi_linux_boot (grub_addr_t k_address, grub_size_t k_size,
   asm volatile ("cli");
 #endif
 
+  /* Invalidate the instruction cache */
+  grub_arch_sync_caches((void *)kernel_addr, kernel_size);
+
   hf = (handover_func)((char *)k_address + h_offset + offset);
   hf (grub_efi_image_handle, grub_efi_system_table, k_params);
 
